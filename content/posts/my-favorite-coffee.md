---
title: My Favorite Coffee
date: 2018-07-06 02:35:56 +0000
description: This is my favorite coffee
post_thumbnail: https://picsum.photos/1800/700/?random

---
My favorite coffee is [Square One](https://www.squareonecoffee.com/ "Square One Coffee") in Lancaster, PA.

![Example image](https://picsum.photos/1200/800/?random)

### Favorite Coffee Roasters

* Square One Coffee
* Passenger Coffee
* Counter Culture
* Stumptown
* Madcap